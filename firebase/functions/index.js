const functions = require("firebase-functions");
const app = require("express")();
const API_PREFIX = "api";
const nodeMailer = require("nodemailer");

app.use((req, res, next) => {
  if (req.url.indexOf(`/${API_PREFIX}/`) === 0) {
    req.url = req.url.substring(API_PREFIX.length + 1);
  }
  next();
});

app.post("/email", (req, res) => {
  let transporter = nodeMailer.createTransport({
    host: "smtp.googlemail.com",
    port: 465,
    secure: true,
    auth: {
      user: "teamwartech@gmail.com",
      pass: "wartech280716"
    }
  });
  let mailOptions = {
    from: '"Finternet" <hello@finternet.co.id>',
    to: "hello@finternet.co.id",
    subject: "Finternet Subscription ✔",
    text: req.body.telepon,
    html: `<b>Ini nomer telponnya : ${req.body.telepon}</b>`
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return console.log(error);
    }
    res.redirect("/");
  });
});

exports[API_PREFIX] = functions.https.onRequest(app);
