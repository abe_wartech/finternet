const {
  override,
  fixBabelImports,
  addDecoratorsLegacy,
  disableEsLint,
  addLessLoader
} = require("customize-cra");

module.exports = override(
  fixBabelImports("import", {
    libraryName: "antd",
    libraryDirectory: "es",
    style: true
  }),
  addDecoratorsLegacy(),
  disableEsLint(),
  addLessLoader({
    javascriptEnabled: true,
    modifyVars: { "@primary-color": "#13c2c2" }
  })
);
