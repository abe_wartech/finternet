import React, { Component } from "react";
import "antd/dist/antd.css";
import "./App.scss";
import {
  Button,
  notification,
  Form,
  Input,
  Divider,
  BackTop,
  Row,
  Col,
  Typography
} from "antd";
import AOS from "aos";
import { Player, BigPlayButton } from "video-react";
import "video-react/dist/video-react.css";
import "aos/dist/aos.css";
const { Paragraph, Title } = Typography;

const finternetintrovideo = require("./assets/finternet-introduction.mp4");
const finternetintro = require("./assets/finternet-introduction.jpeg");
const logo = require("./assets/finternet.svg");
const logosm = require("./assets/logosm.svg");
const maps = require("./assets/maps.png");
const mapssm = require("./assets/mapssm.png");
const location = require("./assets/location.svg");
const dial = require("./assets/dial.svg");
const mail = require("./assets/mail.svg");

class App extends Component {
  state = {
    telepon: ""
  };

  componentDidMount = () => {
    AOS.init({
      duration: 1000,
      delay: 50
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const onChange = name => event => {
      this.setState({
        [name]: event.target.value
      });
    };
    const handleEmail = e => {
      e.preventDefault();
      this.props.form.validateFields((err, values) => {
        if (err) {
          return;
        }
        const { telepon } = this.state;
        const data = {
          telepon: telepon
        };
        setTimeout(() => {
          this.setState({
            telepon: ""
          });
          this.props.form.resetFields();
          notification.open({
            message: "Success",
            description:
              "Permintaan anda akan segera diproses. Kami akan segera menghubungi anda."
          });
        }, 800);

        fetch("api/email", {
          method: "POST",
          headers: {
            "Content-Type": "application/json"
          },
          body: JSON.stringify(data)
        })
          .then(res => res.json())
          .then(res => {
            if (!res.success) {
              this.props.form.resetFields();
              this.setState({
                telepon: ""
              });
              notification.open({
                message: "Gagal",
                description: "Email gagal dikirim"
              });
            }
          })
          .catch(err => console.log(err));
      });
    };
    const rightOrNot = () => {
      if (window.screen.width >= 768) return "right";
      return "";
    };
    return (
      <div>
        <Row className="section1" type="flex" justify="center">
          <Col xs={24} type="flex" justify="center" align="center">
            <div data-aos="zoom-in-up">
              {window.screen.height >= 768 ? (
                <img
                  className="img"
                  alt="finternet"
                  className="logo"
                  src={logo}
                />
              ) : (
                <img
                  className="img"
                  alt="finternet"
                  className="logo"
                  src={logosm}
                />
              )}
            </div>
          </Col>
        </Row>
        <Row className="section2">
          <Col xs={24}>
            <Divider className="divider">
              <Title level={4} className="text-white">
                Internet Untuk Sesama
              </Title>
            </Divider>
          </Col>
        </Row>
        <Row className="section3" type="flex" justify="center">
          <Col xs={24} md={8} sm={12}>
            <div data-aos="fade-right" className="textleft">
              <Player playsInline poster={finternetintro}>
                <BigPlayButton position="center" />
                <source src={finternetintrovideo} type="video/mp4" />
              </Player>
            </div>
          </Col>
          <Col xs={24} md={8} sm={12} className="textright">
            <div data-aos="fade-left">
              <Row>
                <Col className="inside1">
                  <Paragraph className="text-white">
                    Finternet adalah layanan berbagi jaringan internet yang
                    memungkinkan pemilik jaringan{" "}
                    <span className="font-finternet">
                      berbagi internet ke lingkungan sekitar.
                    </span>
                  </Paragraph>
                </Col>
              </Row>
              <Row justify="start">
                <Col className="widthdivide">
                  <Divider className="divide" />
                </Col>
              </Row>
              <Row>
                <Col>
                  <Paragraph className="info">
                    Untuk mendaftarkan daerahmu, daftarkan nomer telepon anda
                  </Paragraph>
                </Col>
              </Row>
              <Row gutter={16}>
                <Form onSubmit={handleEmail}>
                  <Col xs={18}>
                    <Form.Item>
                      {getFieldDecorator("telepon", {
                        type: "number",
                        rules: [
                          {
                            required: true,
                            message: "Silahkan isi no telepon"
                          },
                          {
                            min: 9,
                            message: "No telepon tidak valid"
                          }
                        ]
                      })(
                        <Input
                          className="form"
                          setfieldsvalue={this.state.telepon}
                          size="large"
                          addonBefore="+62"
                          placeholder="822 9090 8080"
                          allowClear
                          onChange={onChange("telepon")}
                          maxLength={14}
                        />
                      )}
                    </Form.Item>
                  </Col>
                  <Col xs={6}>
                    <Button
                      className="button"
                      type="primary"
                      size={"large"}
                      htmlType="submit"
                    >
                      Send
                    </Button>
                  </Col>
                </Form>
              </Row>
              <Row>
                <Col>
                  <Paragraph className="extrainfo">
                    Kami tidak akan membagikan nomor telepon anda ke siapapun.
                  </Paragraph>
                </Col>
              </Row>
            </div>
          </Col>
        </Row>
        <Row className="section4">
          <Col span={24} className="maps">
            <Divider className="divider">
              <Title level={4} className="text-white">
                Cover Area Finternet
              </Title>
            </Divider>
            <div data-aos="zoom-in">
              {window.screen.width >= 768 ? (
                <img className="img" alt="finternet" src={maps} />
              ) : (
                <img className="img" alt="finternet" src={mapssm} />
              )}
            </div>
          </Col>
        </Row>
        <Row type="flex" justify="space-around" className="section6">
          <Col xs={24} md={12}>
            <img src={location} className="icon" />
            <Paragraph className="paragraph">
              Finternet
              <br />
              Jl. Kemang Timur No 12, Mampang Prapatan, Jakarta Selatan
            </Paragraph>
          </Col>
          <Col xs={24} md={10} align={rightOrNot()}>
            <Row>
              <Col xs={3} sm={17} md={16}>
                <img src={mail} />
              </Col>
              <Col xs={21} sm={7} md={8}>
                <Paragraph>hello@finternet.co.id</Paragraph>
              </Col>
            </Row>
            <Row>
              <Col xs={3} sm={17} md={16}>
                <img src={dial} />
              </Col>
              <Col xs={21} sm={7} md={8}>
                <Paragraph>+62 822 2132 3726</Paragraph>
              </Col>
            </Row>
          </Col>
        </Row>
        <Row type="flex" justify="center" className="section5">
          <Col span={24} align="center">
            <Divider />
            Copyright &copy; 2020 Finternet
          </Col>
        </Row>
        <BackTop visibilityHeight={450} />
      </div>
    );
  }
}

export default Form.create()(App);
